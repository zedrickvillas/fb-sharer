<?php
include_once('include/config.php');
include_once('include/components.php');

$appTitle = 'The Health Vibes';
$appDetails = 'Details of the Health Vibes';
$appImage = 'https://www.thehealthvibes.com/assets/favicon/android-icon-192x192.png';

$components = new htmlComponents();
$components->getOpeningHeader();
$components->setAllMeta($appTitle, $appDetails, $appImage , GLOBALS['publication'], GLOBALS['author']);
$components->getClosingHeader();
$components->getPosts(array(),9);
$components->getFooter();
