  // This is called with the results from from FB.getLoginStatus().
  var picHolder;
  var fbAppId = 1840366609615565;
  var userDetails;
  var encryptedDetails;
  (function() {
      console.log(document.getElementsByClassName('single-page').length);
      if (document.getElementsByClassName('single-page').length > 0) {
          console.log('run');
          window.onload = function() {
              setTimeout(function() {
                  document.body.scrollTop = document.documentElement.scrollTop = 45;
              }, 1);
              document.body.scrollTop = document.documentElement.scrollTop = 45;
          };
          setTimeout(function() {
              document.body.scrollTop = document.documentElement.scrollTop = 45;
          }, 1);
      }
  })();
  (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  window.fbAsyncInit = function() {
      FB.init({
          appId: fbAppId,
          cookie: true, // enable cookies to allow the server to access the session
          xfbml: true, // parse social plugins on this page
          version: 'v2.8' // use graph api version 2.8
      });
  };

  function fbLogin() {
      FB.login(function(response) {
          if (response.authResponse) {
              console.log('Permission Success');
              checkLoginState();
          } else {
              //user hit cancel button
              console.log('User cancelled login or did not fully authorize.');
          }
      }, {
          scope: 'user_birthday,user_photos,user_friends,user_posts,publish_actions,public_profile'
      });
  }

  function statusChangeCallback(response) {
      if (response.status === 'connected') {
          // Logged into your app and Facebook.
          
          if (document.getElementById('fbbutton') != null) {
              document.getElementById('fbbutton').style.display = 'none';
          }
          generateImage();
      }
  }

  function checkLoginState() {
      FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
      });
  }

  function generateImage() {
      FB.api('/me?fields=id,name,gender,birthday', function(response) {
          if (typeof(appInfo) != 'undefined') {
              response.appid = appInfo.appID;

              var details = response.id +","+ response.name +","+ response.gender +","+ response.appid;
              encryptedDetails = btoa(details);
              picHolder = appInfo.imgLink + "?fbid=" + encryptedDetails;
              changeImage();
          }
      });
  }

  function changeImage() {
      counter = 0;
      setTimeout(function() {
          document.getElementById('promotion-overlay-container').style.display = "block";
          var fadeIn = setInterval(function() {
              counter = counter + 0.01;
              if (counter > 1) {
                  clearInterval(fadeIn);
              } else {
                  document.getElementById('promotion-overlay-container').style.opacity = counter;
              }
          }, 10)
      }, 300);
      document.getElementById('gen-img-container').classList = "loading";
      setTimeout(function() {
          counter = 1;
          document.getElementById('gen-img-container').classList = "";
          document.getElementById('gen_img').src = picHolder;
          document.getElementById('loginFB').style.display = 'none';
          document.getElementById('sharer').style.display = 'block';
          var fadeOut = setInterval(function() {
              counter = counter - 0.01;
              if (counter < 0) {
                  clearInterval(fadeOut);
                  document.getElementById('promotion-overlay-container').style.display = 'none';
              } else {
                  document.getElementById('promotion-overlay-container').style.opacity = counter;
              }
          }, 10)
      }, 5000);
  }

  function share() {
      FB.ui({
          method: 'feed',
          link: appInfo.link + "?fbid=" + encryptedDetails,
          name: appInfo.appTitle,
          caption: appInfo.appCaption,
          description: appInfo.appDesc,
      }, function(response) {});
  };
