<?php
	include "assets/info.php";
	include_once "../../include/components.php";

	$components = new htmlComponents;
	$components->getOpeningHeader();
	$components->setAllMeta($appTitle, $appDetails, GLOBALS['domain'].'/app/'.$folderName.'/assets/images/sample.png', GLOBALS['publication'], GLOBALS['author']);
	$components->getClosingHeader();
	$components->getAppHeaderScript($folderName, $appId, $imgLink, $appTitle, $appDetails );
	$components->getAppPage($appTitle, $appDetails);
	$components->getPosts(array($folderName), 3);
	$components->getFooter();

?>
