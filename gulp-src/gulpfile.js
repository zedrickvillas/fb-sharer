var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    coffee = require('gulp-coffee'),
    connect = require('gulp-connect'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

var coffeeSources = ['../src/coffee/*.coffee'],
    jsSources = ['../src/js/*.js'],
    sassCompile = ['../src/scss/main.scss'],
    sassSources = ['../src/scss/*/*.scss'],
    jsOutputDir = '../assets/js';
    cssOutputDir = '../assets/css';


gulp.task('log', function() {
  gutil.log('== My First Task ==')
});

gulp.task('sass', function() {
  gulp.src(sassCompile)
  .pipe(sass({style: 'expanded'}))
    .on('error', gutil.log)
  .pipe(gulp.dest(cssOutputDir))
});

gulp.task('coffee', function() {
  gulp.src(coffeeSources)
  .pipe(coffee({bare: true})
    .on('error', gutil.log))
  .pipe(gulp.dest(jsOutputDir))
});

gulp.task('js', function() {
  gulp.src(jsSources)
  .pipe(uglify())
  .pipe(concat('script.js'))
  .pipe(gulp.dest(jsOutputDir))
});

gulp.task('watch', function() {
  gulp.watch(coffeeSources, ['coffee']);
  gulp.watch(jsSources, ['js']);
  gulp.watch(sassSources, ['sass']);
});

gulp.task('default', ['coffee', 'js', 'sass', 'watch']);
