<?php
include_once 'config.php';

class htmlComponents
{

    public $headerCssAssets = [
        'main.css',
    ];

    public $headerJsAssets = [
        // 'script.js',
    ];

    public $footerJsAssets = [
        'script.js',
    ];

    public $globals = GLOBALS;

    public function getOpeningHeader()
    {
        echo '<!doctype html>
                <html class="no-js" lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                        <title>The Health Vibes</title>
                        <meta name="description" content="">
                        <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
                        <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
                        <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
                        <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
                        <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
                        <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
                        <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
                        <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
                        <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
                        <link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
                        <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
                        <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
                        <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
                        <link rel="manifest" href="/assets/favicon/manifest.json">
                        <meta name="msapplication-TileColor" content="#ffffff">
                        <meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
                        <meta name="theme-color" content="#ffffff">
                        <meta name="viewport" content="width=device-width, initial-scale=1">';
        foreach ($this->headerCssAssets as $key => $value) {
            echo "<link rel='stylesheet' href='" . $this->globals["domain"] . "/assets/css/$value'>";
        }

        foreach ($this->headerJsAssets as $key => $value) {
            echo "<script src='" . $this->globals["domain"] . "/assets/js/$value'></script>";
        }

    }

    public function setAllMeta($appTitle, $appDesc, $appImg, $appPub, $appAuthor) {
      echo '
        <meta property="og:type" content="article" />
        <meta property="og:title" content="'. $appTitle .'" />
        <meta property="og:description" content="'. $appDesc .'" />
        <meta property="og:image" content="'. $appImg .'" />
        <meta property="article:publisher" content="'. $appPub .'" />
        <meta property="article:author" content="'. $appAuthor .'" />
      ';
    }

    public function getClosingHeader()
    {
        echo '<script>
                (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

                ga(\'create\', \'UA-93054489-1\', \'auto\');
                ga(\'send\', \'pageview\');

              </script>
          </head>
          <body>
          <header>
            <h5><a href="/" class="logo" data-scroll>The Health Vibes</a><h5>
          </header>
          <div class="container">';
    }

    public function getPosts($exclude = array(), $limit = null)
    {
        echo '<div class="post-container clearfix">';
        $this->globals['enabledApps'] = array_diff($this->globals['enabledApps'], $exclude);
        foreach ($this->globals['enabledApps'] as $key => $app_name) {
            if ($limit > 0) {
                include_once $this->globals['appRoot'] . "/app/" . $app_name . "/assets/info.php";
                if ($key == 1) {
                    ?>
              <div class="app-post">
                <div class="ads-container">

                </div>
              </div>
            <?php }?>
            <a class="app-post" href="/app/<?php echo $app_name ?>">
              <div >
                  <img class="post-icon" src="/assets/images/icon.png"/>
                  <h4 class="app-title"><?=$appTitle?></h4>
                  <!--<p class="app-desc"><?=$appDetails?></p>-->
                  <img class="preview-pic" src="/app/<?php echo $app_name ?>/assets/images/preview.png">

              </div>
            </a>
            <?php
$limit = $limit - 1;
            }
        }
        echo '</div>';
    }

    public function getAppHeaderScript($folderName, $appId, $imgLink, $appTitle, $appDetails)
    {
        if ($imgLink == '') {
            $imgLink = $this->globals["img_gen"];
        }
        echo '
        <script>
          var appInfo = { link:"' . $this->globals["domain"] . '/app/' . $folderName . '", appID:"' . $appId . '", imgLink:"' . $imgLink . '",
          appTitle:"' . $appTitle . '",
          appDesc:"' . $appDetails . '",
          appCaption:"Visit www.thehealthvibes.com now!"};
        </script>
      ';
    }

    public function getAppPage($appTitle, $appDetails)
    {
        echo '
          <div class="promotion-overlay-container" id="promotion-overlay-container">
            <div class="promotion-overlay">
            <div class="promotion-overlay-title">Support Our Partners</div>
              <div class="promotion-fb">
                <h6>Admin Zed<h6>
                <div class="like-container">
                <div class="fb-like" data-href="https://www.facebook.com/adminzed11/?ref=bookmarks" data-width="100px" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="app-post single-page">

            <div id="gen-img-container">
              <div class="loader">Loading</div>
              <img id="gen_img" src="assets/images/sample.png"/>
            </div>

            <h4 class="app-title">' . $appTitle . '</h4>
            <h5 class="app-desc">' . $appDetails . '</h5>
            <button id="loginFB" onclick="fbLogin()" class="loginBtn loginBtn--facebook">
              Login to Start
            </button>

            <button style="display:none;" id="sharer" class="loginBtn loginBtn--facebook" onclick="share()">
            Share on Facebook</button>
          </div>
          
          <h4 class="text-center">- Try Other Apps -</h4>
      ';

    }

    public function getFooter()
    {
        echo '</div>
                <footer class="footer">
                  <p>&copy; Company 2015</p>
                  <div class="footer-menu">
                  <ul>
                          <li class="menu-item active"><a href="/" data-scroll>Home</a></li>
                          <li class="menu-item"><a href="/pages/about" data-scroll>About</a></li>
                          <li class="menu-item"><a href="/pages/disclaimer" data-scroll>Disclaimer</a></li>
                          <li class="menu-item"><a href="/pages/privacy" data-scroll>Privacy</a></li>
                          <li class="menu-item"><a href="/pages/tos" data-scroll>Terms</a></li>
                        </ul>
                        </div>
                </footer>
          ';
        foreach ($this->footerJsAssets as $key => $value) {
            echo "<script src='" . $this->globals["domain"] . "/assets/js/$value'></script>";
        }
        echo '
      </body>
      </html>
      ';
    }

}
?>
