<?php

	class img_creator {

		protected $image;
		protected $image_type;

		public function __construct( array $config = [] ) {
			if ( isset($config['template']) ) {
            	$this->load_img('assets/images/'.$config['template']);
			}

		}

		public function load_img($template) {
			$image_info = getimagesize($template);
			$this->image_type = $image_info[2];
			if ($this->image_type == IMAGETYPE_JPEG) {
				$this->image = imagecreatefromjpeg($template);
			} elseif ($this->image_type == IMAGETYPE_GIF) {
				$this->image = imagecreatefromgif($template);
			} elseif ($this->image_type == IMAGETYPE_PNG) {
				$this->image = imagecreatefrompng($template);
			} else {
				throw new Exception("The file you're trying to open is not supported");
			}
		}

		public function insert_text($text, $font, $font_size, $font_color_hex = "#000000", $pos_x = 0, $pos_y = 0){
			list($r, $g, $b) 	= sscanf($font_color_hex, "#%02x%02x%02x");
			$font_color 		= ImageColorAllocate($this->image, $r, $g, $b);
			imagettftext($this->image, $font_size, 0, $pos_x, $pos_y, $font_color, 'assets/fonts/'.$font, $text);
		}

		public function insert_img_url($img_url, $width, $height, $pos_x, $pos_y){

			$url_to_img = imagecreatefromjpeg($img_url);
			$size		= getimagesize($img_url);
			$resized_img = imagecreatetruecolor($width, $height);
			imagecolortransparent($resized_img, imagecolorallocate($resized_img, 0, 0, 0));
			imagealphablending($resized_img, false);
			imagesavealpha($resized_img, true);
			imagecopyresampled($resized_img, $url_to_img, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
			imagecopymerge($this->image, $resized_img, $pos_x, $pos_y, 0, 0, $width, $height, 100);

		}

		public function save($filename, $image_type = IMAGETYPE_JPEG, $compression = 75) {
			if ($image_type == IMAGETYPE_JPEG) {
				imagejpeg($this->image,'generate/'.$filename,$compression);
			} elseif ($image_type == IMAGETYPE_GIF) {
				imagegif($this->image,'generate/'.$filename);
			} elseif ($image_type == IMAGETYPE_PNG) {
				imagepng($this->image,'generate/'.$filename);
			}
		}

		public function output($image_type=IMAGETYPE_JPEG, $quality = 80) {
			if ($image_type == IMAGETYPE_JPEG) {
				header("Content-type: image/jpeg");
				imagejpeg($this->image, null, $quality);
			} elseif ($image_type == IMAGETYPE_GIF) {
				header("Content-type: image/gif");
				imagegif($this->image);
			} elseif ($image_type == IMAGETYPE_PNG) {
				header("Content-type: image/png");
				imagepng($this->image);
			}
		}

	}

?>
