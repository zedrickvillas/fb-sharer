<?php

// Path Process
$pathArray = explode('/', dirname(__FILE__));
$pathArray[sizeof($pathArray)-1] = '';
$path = implode('/', $pathArray) ;

define(
  'GLOBALS', [
    'domain' => 'http://'.$_SERVER['HTTP_HOST'],
    'appRoot' => $_SERVER['DOCUMENT_ROOT'],
    'img_gen' => 'http://image.wackyoink.com/imggen.php',
    'cdnDomain' => '',
    'assetsPrefix' => '\/assets\/',
    'docRoot' => $path,
    'enabledApps' => [
      'personal-id',
      'past-animal',
    ],
    'publication' => 'https://www.facebook.com/The-Health-Vibes-609847455882093/',
    'author' => 'https://www.facebook.com/The-Health-Vibes-609847455882093/',

]);
