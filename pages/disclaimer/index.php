<?php
    include_once('../../include/config.php');
    include_once('../../include/components.php');

    $components = new htmlComponents();
    $components->getHeader();
?>

<div class="inner_page">
<h4>Disclaimer</h4>
<p><strong>The Health Vibes</strong> inspires people through its generated images and brings joy to its visitors. The generated images that thehealthvibes provide are random and are just based on the data that the Facebook integration provide. We do not take have personal affiliation with any other organization that may affect our generated image. The results are purely based on randomization that our algorithm has. </p>

<p>We are very welcoming in any inquiry from the visitors and ready to answer the queries received.</p>

<p>Aside from that, <strong>The Health Vibes</strong> goal is just to bring joy to our visitors and give them a break with the daily stress that we encounter. Our generated images hopes to bring happiness to many.</p>
</div>
<?php
$components->getFooter();
